FROM python:3.8

RUN pip3 install pipenv

WORKDIR /usr/src/category-checker-py

COPY Pip* ./

RUN pip install --upgrade pip && \
  pip install pipenv && \
  pipenv install --dev --system --deploy --ignore-pipfile

# Bundle app source
COPY . .

EXPOSE 5000

CMD flask run --host=0.0.0.0