from .db import db


class Category(db.Document):
    name = db.StringField(required=True, unique=True)
    meta = {'collection': 'categories', 'strict': False}


class Website(db.Document):
    url = db.StringField(required=True, unique=True)
    categoryId = db.ReferenceField(Category)
    meta = {'collection': 'websites', 'strict': False}
