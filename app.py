
import os
from flask import Flask, request, jsonify
from database.db import initialize_db
from database.models import Category, Website

app = Flask(__name__)

app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']
}

initialize_db(app)


@app.route('/')
def index():
    return 'Server Works!'


@app.route('/categories')
def get_categories():
    categories = Category.objects()
    return jsonify(categories)


@app.route('/websites')
def get_website_category():
    try:
        url = request.args.get('url')
        website = Website.objects.get(url=url)
        return jsonify(category=website.categoryId.name)
    except Website.DoesNotExist:
        return jsonify({})


app.run()
